var atomicalgolia = require("atomic-algolia")

var indexes = [
    {
        name: "pixeLfix.en",
        path: "public/en/index.json"
    },
    {
        name: "pixeLfix.ko",
        path: "public/index.json"
    }
]
var cb = function (error, result) {
    if (error) throw error

    console.log(result)
}

indexes.forEach(function(index) {
  atomicalgolia(index.name, index.path, cb);
});